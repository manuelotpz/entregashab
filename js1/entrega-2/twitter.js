/**
 * El objetivo del ejercicio es crear un nuevo array que contenga
 * todos los hashtags del array `tweets`, pero sin repetir
 * 
 * Nota: como mucho hay 2 hashtag en cada tweet
 */

tweets = [
    'aprendiendo #javascript en  Vigo', 
    'empezando el segundo módulo del bootcamp!',
    'hack a boss bootcamp vigo #javascript #codinglive']
    
    output = []


    for (tweet of tweets) {
        start = tweet.indexOf('#')
        end = tweet.indexOf(' ', start)
        next = tweet.indexOf('#', end)
        for (i = 0; i < tweet.length; i++) {
            if (tweet[i] == '#') {
                output.push(tweet.slice(start, end))
                start = next
            }
        }
    }
    console.log(output)

