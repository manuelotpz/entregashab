/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir eventos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */

const bodyParser = require('body-parser');
const express = require('express');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let listaEventos = []
app.post('/listaEventos', (req, res) => {

    let data = {
        tipo: req.body.tipo,
        name: req.body.name,
        aforo: req.body.aforo,
        artista: req.body.artista
    }

    let search = listaEventos.filter(event => event.name === req.body.name)
    if(req.body.tipo !== 'teatro' && req.body.tipo !== 'concierto' && req.body.tipo !== 'monologo'){
        res.status(400).send('Uno de los campos no es válido');
        return;
    }
    if  (req.body.tipo === undefined ||
        req.body.name === undefined || 
        req.body.aforo === undefined ||
        req.body.artista === undefined) {
        res.status(400).send('Te faltan uno o más campos');
        return;
    }

    if (search.length !== 0){
        res.status(409).send('Este concierto ya está registrado');
        return;
    }
    else {
        listaEventos.push(data);
        res.status(200).send();
    }
})

app.get('/listaEventos', (req, res) => {
   res.json(listaEventos);
})

app.listen(3000)
