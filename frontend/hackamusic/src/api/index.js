import config from './config.js';
const axios = require('axios').default;

const apiKey = config.apiKey

const BASE_URL = "https://ws.audioscrobbler.com/"
const URL_GEO = "2.0/?method=geo.gettopartists&country=spain&api_key="+apiKey+"&format=json"

const TOPTRACKS_URL = "https://ws.audioscrobbler.com/"
const URL_TOPTRACKS = "2.0/?method=geo.gettoptracks&country=spain&api_key="+apiKey+"&format=json"

const TOPTAGS_URL = "https://ws.audioscrobbler.com/"
const URL_TOPTAGS = "2.0/?method=chart.gettoptags&api_key="+apiKey+"&format=json"


async function getArtists(){
    try {
        const response = await axios.get(`${BASE_URL}${URL_GEO}`);
        return response;
    } catch (error){
        console.error(error);
    }
}

async function getTracks(){
    try {
        const response = await axios.get(`${TOPTRACKS_URL}${URL_TOPTRACKS}`);
        return response;
    } catch (error){
        console.error(error);
    }
}

async function getTopTags(){
    try {
        const response = await axios.get(`${TOPTAGS_URL}${URL_TOPTAGS}`);
        return response;
    } catch (error){
        console.error(error);
    }
}


export default {
    getArtists,
    getTracks,
    getTopTags
}