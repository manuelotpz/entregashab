-- Entregable:

-- usuarios (nombre, apellidos, direccion, e-mail, contrasena) -- ok
-- reservas (restaurante, dia, hora, duracion en establecimiento)
-- encargados de restaurantes (email, contrasena, nombre, direccion, apellidos) -- ok
-- restaurantes (nombre, direccion, capacidad maxima)
-- datos grupos (nombres, apellidos, DNI)

USE entregables;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE usuarios (
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    direccion VARCHAR(100),
    email VARCHAR(50) UNIQUE NOT NULL,
    contrasena VARCHAR(40) NOT NULL
);

CREATE TABLE reservas (
    restaurante VARCHAR(50) NOT NULL,
    dia DATETIME,
    hora TINYINT UNSIGNED,
    duracion_en_establecimiento TINYINT UNSIGNED
);

CREATE TABLE encargados_de_restaurante (
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    direccion VARCHAR(100),
	email VARCHAR(50) UNIQUE NOT NULL,
    contrasena VARCHAR(40) NOT NULL
);

CREATE TABLE restaurantes (
	nombre VARCHAR(50),
    direccion VARCHAR(100),
    capacidad_maxima TINYINT 
);


CREATE TABLE datos_grupo (
    dni VARCHAR(9) UNIQUE,
    nombre VARCHAR(50),
    apellidos VARCHAR(50)
);