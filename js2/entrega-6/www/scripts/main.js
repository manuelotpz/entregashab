
const borrarElemento = e => {
    if (e.target.classList.contains('delete')) {
        e.target.parentElement.remove()
    }
}

const buttons = document.querySelectorAll('.button')
const list = document.getElementById('list')

const addToList = elemento => {
    list.appendChild(elemento)
}

const addHotel = elemento => {
    const li = document.createElement('li')
    // Los cambios de última hora han sido desde aquí
    li.innerHTML = `
        <img src ="${elemento.imagen}">
        <section> 
        <h2 class="name">${elemento.nombre}<h2>
        <h2 class="precio">${elemento.precio}<h2>
        </section>
    `
    // hasta aquí
    list.appendChild(li)
    
    const button = document.createElement('button')
    button.classList = 'delete'
    button.textContent = 'x'
    li.appendChild(button)

    addToList(li)
}
const extraerDatosHotel = hotel => {
    const datosHotel = {
        imagen: hotel.querySelector('img').src,
        nombre: hotel.querySelector('h1').innerText,
        precio: hotel.querySelector('.price').innerText
    }
    addHotel(datosHotel)
}

const enviarHotel = e => {
    e.preventDefault()
    const elemento = e.target.parentElement.parentElement
    extraerDatosHotel(elemento)
}

// Listeners
buttons.forEach(button => {
    button.addEventListener('click', enviarHotel)
}
)

list.addEventListener('click', borrarElemento)